# Written by Elijah Kennedy

# Setup

Clone this repo locally if you haven't already:

```
git clone https://gitlab.com/daniel.barker/project-alpha-apr.git project-alpha-apr
```

And `cd` into it with `cd project-alpha-apr`.

Create a virtual environment if there isn't one already:

```
python -m venv venv
```

Activate the virtual environment:

```
source venv/bin/activate
```

Now you can install the requirements:

```
pip3 install -r requirements.txt
```

Awesome. Now that the requirements are installed, including django, let's take it for a spin.

```
python3 manage.py test
```

If all the tests were successfully, let's do the following to run the app:

```
python3 manage.py migrate;
python3 manage.py runserver;
```

The output of which should tell you were you can view this project! Head over there to check it out.
